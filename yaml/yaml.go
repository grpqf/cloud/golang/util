package yaml

import (
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

func GenerateYamlToFile(path string, i interface{}) string {
	log.SetFlags(log.Lshortfile)

	usersvars, err := yaml.Marshal(i)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	file, err := os.Create(path + "/user.yaml")
	if err != nil {
		log.Fatalln(err)
	}

	defer file.Close()

	file.Write(usersvars)

	return file.Name()
}

func GenerateStructFromYaml(file []byte, i interface{}) {
	errFile := yaml.Unmarshal(file, i)
	if errFile != nil {
		log.Println("Ocorreu um erro na geração do yaml")
		log.Fatal(errFile.Error())
	}
}
