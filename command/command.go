package command

import (
	"os"
	"os/exec"
	"util/structure"
)

func Exec(pathPlb, command string, args ...string) {
	doCmd := exec.Command(command, args...)
	doCmd.Stdout = os.Stdout
	doCmd.Stderr = os.Stderr

	err := doCmd.Run()

	if e, ok := err.(*exec.ExitError); ok && !e.Success() {
		structure.CleanAll(pathPlb)
		os.Exit(1)
	}
}
