package errors

import (
	"log"
)

func Check(target string, err error) {
	if err != nil {
		log.Println("Target: " + target)
		log.Println("Ocorreu um erro fatal e o sistema sera encerrado")
		log.Fatal(err.Error())
	}
}
