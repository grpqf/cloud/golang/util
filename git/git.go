package git

import (
	"fmt"
	"os"
	"util/structure"

	"util/errors"

	git "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/http"
)

func Clone(repo, branchName, path, user, pass string) {
	_, err := git.PlainClone(path+"/."+repo, false, &git.CloneOptions{
		Auth: &http.BasicAuth{
			Username: user,
			Password: pass,
		},
		URL:           "https://bitbucket.org/maxmilhas/" + repo + ".git",
		ReferenceName: plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", branchName)),
		SingleBranch:  true,
		Progress:      os.Stdout,
	})

	errors.Check(repo, err)
}

func CloneFromTag(repo, tagName, path, user, pass string) {
	_, err := git.PlainClone(path+"/."+repo, false, &git.CloneOptions{
		Auth: &http.BasicAuth{
			Username: user,
			Password: pass,
		},
		URL:           "https://bitbucket.org/maxmilhas/" + repo + ".git",
		ReferenceName: plumbing.ReferenceName(fmt.Sprintf("refs/tags/%s", tagName)),
		SingleBranch:  true,
		Progress:      os.Stdout,
	})

	errors.Check(repo, err)
}

func CloneRepo(repository, path, user, pass string) {

	_, err := git.PlainClone(path+"/."+repository, false, &git.CloneOptions{
		Auth: &http.BasicAuth{
			Username: user,
			Password: pass,
		},
		URL:      "https://bitbucket.org/maxmilhas/" + repository + ".git",
		Progress: os.Stdout,
	})

	errors.Check(repository, err)
}

func CreateStructure(path string) string {
	newPath := structure.GenerateRandomPath(path)

	errors.Check("Criando estrutura "+path, os.MkdirAll(path, os.ModePerm))

	return newPath
}
