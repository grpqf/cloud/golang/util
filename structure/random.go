package structure

import (
	"log"
	"math/rand"
	"time"

	"github.com/satori/go.uuid"
)

func GenerateRandomUsr(length int) string {
	rand.Seed(time.Now().UnixNano())
	lengthr := rand.Intn(length-(length-5)) + (length - 5)

	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

	b := make([]byte, lengthr)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}

	return string(b)
}

func GenerateRandomPath(path string) string {
	uuid, err := uuid.FromString(uuid.Must(uuid.NewV4()).String())

	if err != nil {
		log.Fatal(err)
	}

	return path + "/." + uuid.String()
}
