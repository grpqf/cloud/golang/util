package structure

import (
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"util/errors"
)

func createBaseDir(pathDir string) {
	if _, err := os.Stat(pathDir); os.IsNotExist(err) {
		err = os.MkdirAll(pathDir, 0700)
		if err != nil {
			panic(err)
		}
	}
}

func CreateServiceDirectory(path string) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, rand.Intn(len(letterRunes)))
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}

	dir, err := ioutil.TempDir(path, string(b))
	if err != nil {
		log.Fatal(err)
	}

	return dir
}

func CleanAll(path string) {
	os.RemoveAll(path)
}

func CopyFile(src, dest string) {
	if _, err := os.Stat(src); err != nil {
		if os.IsNotExist(err) {
			return
		}
	}

	data, err := ioutil.ReadFile(src)
	errors.Check("Copiando arquivo "+src, err)

	err = ioutil.WriteFile(dest, data, 0700)
	errors.Check("Escrevendo arquivo "+dest, err)
}
