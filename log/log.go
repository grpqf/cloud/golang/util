package log

import (
	"os"
	"strings"
	"time"
	"util/errors"
)

func WriteLogOut(content string) {
	appendLogFile(content, "out.log")
}

func WriteLogErr(content string) {
	appendLogFile(content, "err.log")
}

func validatePathLog() string {
	hkPathDir := "/var/log/hk"
	_, err := os.Stat(hkPathDir)

	if err != nil && strings.Contains(err.Error(), "no such file or directory") {
		errors.Check(os.Mkdir(hkPathDir, os.ModePerm))
	}

	return hkPathDir
}

func appendLogFile(content, logName string) {
	hkPathDir := validatePathLog()

	hkPathFile := hkPathDir + "/" + logName
	_, hklogErr := os.Stat(hkPathFile)

	var file *os.File
	var err error

	if hklogErr != nil && strings.Contains(hklogErr.Error(), "no such file or directory") {
		file, err = os.Create(hkPathFile)
		errors.Check(err)
	} else {
		file, err = os.OpenFile(hkPathFile, os.O_APPEND|os.O_RDWR, 0600)
		errors.Check(err)
	}

	defer file.Close()

	file.WriteString("\n\n")
	file.WriteString("Date: " + time.Now().Format(time.RFC850))
	file.WriteString("\n")
	file.WriteString(content)
}
